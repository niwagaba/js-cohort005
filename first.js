// Our first Js file
var name = 'rogers';

var age = 451;
var gender;
// Let’s say you’re trying to use quotation marks inside a string. You’ll need to use opposite quotation marks inside and outside
let sent = "That's for Ozzy";
let recieve = 'he said, "Get out of here"'

// properties of strings (They use a .method(); at the end of the string)
// prperty to show the length
"understand".length;

"The food".toLowerCase();

// NUMBERS
// They dont need any syntax, just write them direct
1 + (4/5);

// BOOLEANS
// true or false yes/no on/off

// Operators- these are symbols between values +,-,/,*,=
let a = 354534;
let b =34322;
let c = a+b;

console.log( a + b);
console.log(c);

// grouping; these include operator groups. the code executes things on the left first
console.log((a/b)+c);

//concatenation
// We use + to join strings
let d = "What a boring morning";
let e = 'Awesome evening for John"s cat';

var f = d + e;
console.log(f);
//  The assignment operator is used to assign variables to values
var dinner = "katogo";




//please output this on the screen

console.log('Whats good');

//Lets now dive into variables
//Variables are named values and can store any type of Javascript value
var weather = "Sunny";

//Reassigning the values
weather = "rainy";

console.log(weather);
//Variables are case senstive and its better to use unsimilar variable to make life easier

//FUNCTIONS
//These are blocks of code that can be named and reused
let ageRogers = 24;
let ageNiwagaba = 25;

//we use the return statement to get back the answer

function multiplyTheNumbers(ageNiwagaba, ageRogers) {
    return ageNiwagaba * ageRogers;
};

/* function(keyword declaring function)
multiplyTheNumbers is the name of the function which can be called any where
(ageNiwagaba,ageRogers) are parameters; they are variable names that a function will accept, they are not necessary tho
return is a keyword exiting the fuction and can shajl
    return ageRogers + ageRogers ;
};

//When calling a function you type fuctionName();
reduceAge();

//Need to do more research on parameters and if functions can use external valuables


//JAVASCRIPT CONDITIONAL STATEMENTS- control the bahaviour in javascript and tell if pieces of code are to run or not



