// Dealing with math problems

var num1 = 3, num2 = 7, sum;
sum = num2 + num2;
console.log(sum);

//functions
function add(){
    var num1 = 3, num2 = 7, sum;
    sum = num2 + num2;
    console.log(sum);
}

add();

//Another way of dealing with it
var num3 = 4;
var num4 = 9;
var num5 = 2;
var num6 = num4 + num3;

//another way of doing it
var num7;
var num8;
var num9;
num8 = 3; num7 =7;
num9 = num8 + num7;
console.log(num9);

//another way
var num10 =3, num11 = 7;
var num12 = num10 + num11;
console.log(num12);


function sub(){

    var num1 = 3, num2 = 7, sum;
    sum = num1 - num2;
    console.log(sum);
}

sub();













/* var nam1 = "20";

console.log(typeof(nam1));

//operators in Javascript

var num1 = 100;
var num2 = 200;
var num3;

//another way of declaring 
let num4 = 300;
let num5 = 400;

//another way of declaring variables that do not change
const num6 = 500;

// dealing with constant variable
//num6 = 600;

//console.log(num6);

var num7 = num1 % num2; 

console.log(num7);

// the two stars mean exponation(power)
var num8 = num1 ** 2;//num1 power2 / num1squared
console.log(num8);

//comparing variables
console.log(num3 ==num2);//its going to bring false the are not the same / a comparison operator has been used
console.log(num1===num2);//compares datatypes
//checking datatypes
console.log(typeof(num6));

//writing NOT in programming
console.log (num1!=num2); //We use the exclamation mark!
console.log (num5<=num4); // less or equal to

var num9 = num2 += num5; // num2 = num2 + num3
console.log(num9);

var num10 = (num2<num4)?num7:num8; */