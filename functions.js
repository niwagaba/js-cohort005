function add(num1, num2){ //with parameters
    var ans = num1 + num2;
    //ans is just a global variable
    // num1 & num2 are just parameters
    console.log(ans);
}

//without parameters
function add2(){
    var num1 = 10, num2 = 21;
    var ans = num2 + num1;
    
    console.log(ans);
}

//The one with parameter needs arguments to be passed inside the function to run. 
add(29, 27);
// the one without parameters can run in vanilla form
add2();
// if you put 3 of them, the 3rd is ignored
add(3, 9, 4);